<h1>The Everyday Django Starter Pack</h1>
<br>
<p>
  <a href="https://gitlab.com/andrewgy8/django-starter-pack">
    <img alt="creditml" title="creditml" src="https://www.svgrepo.com/show/13920/boiler.svg" width="200">
  </a>
</p>

<p>
  A quick and easy to use Django creditml ready for production in 10 min. No bloat guaranteed.
</p>

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Getting Started](#getting-started)

## Introduction

[![pipeline status](https://gitlab.com/andrewgy8/django-starter-pack/badges/main/pipeline.svg)](https://gitlab.com/andrewgy8/django-starter-pack/-/commits/main)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](http://commitizen.github.io/cz-cli/)

This opinionated creditml is intended for people who wish to develop a Django app
and get it deployed to production as fast as possible with minimal friction.

This is not a project which has all the latest frontend frameworks integrated,
or the latest packages installed. Self-admittedly, it is a boring, no glam starting point
to a production app.

It promises simplicity, extensibility and the ability to deliver value as fast as possible.

## Features

In this starter pack, you can find:

* Django, Postgres and Heroku deployments with zero downtime
* [Pico Css](https://picocss.com/)
* Gitlab CI/CD
* Sentry integration
* An intuitive Makefile for easy local dev

## Getting Started

For a quick video, please see

[![](http://img.youtube.com/vi/kRtiXF0asfA/0.jpg)](http://www.youtube.com/watch?v=kRtiXF0asfA)

My Assumptions about **your** env:
- Heroku setup and can run `heroku` cli commands
- Docker installed
- Gitlab account

> Note: Please replace `<PROJECT_NAME>` with your actual project name. It should not contain spaces or special characters.

1. `git clone git@gitlab.com:andrewgy8/django-starter-pack.git <PROJECT_NAME>`
2. Case-insensitive, match-case replace all `creditml` with `<PROJECT_NAME>`
3. `rm -rf .git && git init && git add . && git commit -am 'First commit'`

### Local Development
1. `make env-recreate`
2. `make bash`
   1. From here, you can run `pytest`, `flake8`, `isort`
   2. `python manage.py createsuperuser`
3. Navigate to [http://localhost:8050/](http://localhost:8050/)

### Production Deploy
1. `heroku apps:create <PROJECT_NAME> && heroku addons:create heroku-postgresql:hobby-dev && heroku stack:set container`
2. Update all the env vars in Heroku
   1. Required variables include
      1. `DJANGO_SETTINGS_MODULE` (`<PROJECT_NAME>.settings.deployment`)
      2. `SECRET_KEY`
3. `git push heroku master`
4. `heroku open`


### CI/CD Setup
1. Create a new [Gitlab repo](https://gitlab.com/projects/new)
2. Set up the variables in Gitlab --> Settings --> CI/CD --> Variables
   1. `HEROKU_APP_PRODUCTION` --> `<PROJECT_NAME>`
   2. `HEROKU_API_KEY` --> `heroku authorizations:create` (copy the token, not the id)
3. `git push --upstream origin main`

