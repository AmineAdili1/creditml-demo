import os

PORT = os.environ.get('PORT', 8000)
bind = f'0.0.0.0:{PORT}'
workers = 2
timeout = 60
name = 'creditml'
errorlog = '-'
loglevel = 'info'
accesslog = '-'
access_log_format = '%(h)s %(l)s %(u)s [%(t)s] "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s" "%({X-Forwarded-For}i)s" %({X-Request-Id}i)s %({Host}i)s'
