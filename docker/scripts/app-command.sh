#!/usr/bin/env bash

exec gunicorn creditml.wsgi:application \
  --config /var/gunicorn/gunicorn.py \
  "$@"
