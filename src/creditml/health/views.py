from django.http import JsonResponse
from django.views.decorators.http import require_http_methods


@require_http_methods(["GET"])
def system_health(request):
    return JsonResponse({'status': 'ok'})


system_health.login_exempt = True
