"""
App specific configurations.
"""

SPECTACULAR_DEFAULTS = {
    'TITLE': 'creditml',
    'DESCRIPTION': 'creditml API',
    'SERVERS': [],
}
