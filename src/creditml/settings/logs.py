import os
from logging import config as logging_config

LOGGING_CONFIG = None
logging_config.dictConfig({
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'generic': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s",  # noqa
        },
        'json': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s %(lineno)d %(pathname)s',  # noqa
            'class': 'pythonjsonlogger.jsonlogger.JsonFormatter'
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': os.environ.get('creditml_LOG_FORMATTER', 'json'),
        },
    },
    'loggers': {
        '': {
            'level': 'INFO',
            'handlers': ['console'],
        },
        'django': {
            'level': os.environ.get('DJANGO_LOG_LEVEL', 'INFO'),
            'handlers': ['console'],
            'propagate': False,
        },
        'gunicorn.access': {
            'level': 'WARNING',
            'handlers': ['console'],
        },
        'gunicorn.error': {
            'level': 'ERROR',
            'handlers': ['console'],
        }
    },
    'root': {
        'level': 'INFO',
        'handlers': ['console'],
    },
})
