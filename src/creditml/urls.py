from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from django.views.generic import TemplateView

urlpatterns = [
    # Template View Urls
    path('', TemplateView.as_view(template_name='base.html')),
    path('admin/', admin.site.urls),
    path('health/', include('creditml.health.urls')),

    # API Urls
    path('api/docs/', include('creditml.docs.urls')),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
        path('rosetta/', include('rosetta.urls'), name='rosetta'),
    ] + urlpatterns
