FROM python:3.8.5-slim

COPY docker/app/assets/gunicorn/* /var/gunicorn/

RUN apt-get update \
    && apt-get -y install gettext \
    && rm -rf /var/lib/apt/lists/*

COPY ./src /app/src
COPY docker/scripts /app/scripts
COPY ./requirements /app/requirements

RUN pip install --no-cache-dir --disable-pip-version-check -r /app/requirements/production.txt

WORKDIR /app/src

RUN python manage.py collectstatic --noinput

RUN python manage.py compilemessages

EXPOSE 8000
