#!/usr/bin/make -f

PROJECT_NAME := 'creditml'

.DEFAULT_GOAL := help

.PHONY: install-test-requirements test linting build
.PHONY: env-start env-stop env-recreate docker-cleanup migrations migrate bash shell
.PHONY: changelog prepare-deploy view-logs help

creditml_ROOT_FOLDER := $(shell pwd)
DOCKER_COMPOSE_FILE := $(creditml_ROOT_FOLDER)/docker/docker-compose.yaml
creditml_SERVICE := app
DOCKER_PROJECT_NAME := creditml

ifeq ($(DOCKER_NAMESPACE),)
export DOCKER_NAMESPACE := creditml
endif

ifeq ($(DOCKER_IMAGE_NAME),)
export DOCKER_IMAGE_NAME := creditml
endif

ifeq ($(DOCKER_IMAGE_TAG),)
export DOCKER_IMAGE_TAG := latest
endif

ifeq ($(DOCKER_BRANCH_NAME),)
export DOCKER_BRANCH_NAME := local
endif

install-test-requirements: ## Install all test dependencies
	docker-compose -p $(DOCKER_PROJECT_NAME) -f $(DOCKER_COMPOSE_FILE) exec -T $(creditml_SERVICE) pip install --disable-pip-version-check -r /app/requirements/test.txt

test: ## Run test suite in project's main container
	docker-compose -p $(DOCKER_PROJECT_NAME) -f $(DOCKER_COMPOSE_FILE) exec -T $(creditml_SERVICE) /app/scripts/test-command.sh

docs: ## Generate schema for api docs
	docker-compose -p $(DOCKER_PROJECT_NAME) -f $(DOCKER_COMPOSE_FILE) exec $(creditml_SERVICE) python ./manage.py spectacular --file creditml/docs/schema.yml

linting: ## Check/Enforce Python Code-Style
	docker-compose -p $(DOCKER_PROJECT_NAME) -f $(DOCKER_COMPOSE_FILE) exec -T $(creditml_SERVICE) /app/scripts/lint-command.sh $(LINTFLAGS)

env-start: ## Start project containers defined in docker-compose
	docker-compose -p $(DOCKER_PROJECT_NAME) -f $(DOCKER_COMPOSE_FILE) up -d

env-stop: ## Stop project containers defined in docker-compose
	docker-compose -p $(DOCKER_PROJECT_NAME) -f $(DOCKER_COMPOSE_FILE) stop

env-destroy: ## Destroy all project containers
	docker-compose -p $(DOCKER_PROJECT_NAME) -f $(DOCKER_COMPOSE_FILE) down -v --rmi all --remove-orphans

env-recreate: build env-start install-test-requirements migrate  ## Force building project image and start all containers again

env-reset: destroy-containers env-start install-test-requirements migrate ## Destroy project containers and start them again

destroy-containers: ## Destroy project containers
	docker-compose -p $(DOCKER_PROJECT_NAME) -f $(DOCKER_COMPOSE_FILE) down -v

docker-cleanup: ## Purge all Docker images in the system
	docker-compose -p $(DOCKER_PROJECT_NAME) -f $(DOCKER_COMPOSE_FILE) down -v
	docker system prune -f

migrations: ## Create migration files that match the current status of the Django models defined in project's main container
	docker-compose -p $(DOCKER_PROJECT_NAME) -f $(DOCKER_COMPOSE_FILE) exec -T $(creditml_SERVICE) python manage.py makemigrations $(CHECKFLAG)

migrate: ## Execute all pending Django migrations in project's main container
	docker-compose -p $(DOCKER_PROJECT_NAME) -f $(DOCKER_COMPOSE_FILE) exec -T $(creditml_SERVICE) python manage.py migrate

bash: ## Open a bash shell in project's main container
	docker-compose -p $(DOCKER_PROJECT_NAME) -f $(DOCKER_COMPOSE_FILE) exec $(creditml_SERVICE) bash

shell: ## Open a Django shell in project's main container
	docker-compose -p $(DOCKER_PROJECT_NAME) -f $(DOCKER_COMPOSE_FILE) exec $(creditml_SERVICE) python manage.py shell_plus

view-logs: ## Display interactive logs of all project containers
	docker-compose -p $(DOCKER_PROJECT_NAME) -f $(DOCKER_COMPOSE_FILE) logs -f

help: ## Display this help text
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
